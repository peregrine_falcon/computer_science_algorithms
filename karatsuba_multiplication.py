# Case 1 large number having even number of digits

def num_half_split(x, y):
    num1_str = str(x)
    num2_str = str(y)
    l1 = len(num1_str) 
    l2 = len(num2_str)
    
    if l1 >= l2:
        # pad zeroes for the smaller string
        for _ in range(l1 - l2):
            num2_str = '0' + num2_str
    else:
        # pad zeroes for the smaller string
        for _ in range(l2 - l1):
            num1_str = '0' + num1_str

    # both lengths will be same after padding
    split = len(num1_str) // 2 + len(num1_str) % 2
    
    return (int(num1_str[ : split]), 
            int(num1_str[split : ]), 
            int(num2_str[ : split]), 
            int(num2_str[split : ]))
    
# print(num_half_split(1234, 5678))

def karatsuba_mul(x, y):
    if x <= 10 or y <= 10:
        return x * y
    else:
        N = max(len(str(x)), len(str(y)))

        a, b, c, d = num_half_split(x, y)   
        
        s1 = karatsuba_mul(a, c)
        s2 = karatsuba_mul(b, d)
        s3 = karatsuba_mul(a + b, c + d) - s1 - s2

        return (10 ** (2 * (N // 2))) * s1 + (10 ** (N // 2)) * s3 + s2

# print(karatsuba_mul(1234, 6745), 1234 * 6745)
# print(karatsuba_mul(1234, 78), 1234 * 78)
print(karatsuba_mul(1234, -12), 1234 * (-12))
