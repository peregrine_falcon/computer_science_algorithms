def gen_3_seq(elements):
    seq = []

    for i in [a for a in elements]:
        for j in [b for b in elements if b != i]:
            for k in [c for c in elements if c != i and c != j]:
                seq.append(str(i) + str(j) + str(k))
    return seq
    
print(gen_3_seq([0, 1, 2]))
