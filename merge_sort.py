import random
# implementation of merge sort
# O(n) = nlog(n)

A = [4, 2]
B = [4, 3, 1, 2]
C = random.sample(range(1, 100), random.randint(1, 20))

def merge_sort(number_list):
    # base case
    list_length = len(number_list)
    if list_length < 2:
        return number_list
    
    split = list_length // 2 + list_length % 2
    
    # left, right list copies
    left = number_list[:split]
    right = number_list[split:]

    # sort the left half 
    merge_sort(left)

    # sort the right half
    merge_sort(right)

    # merge the two halves    
    merge(number_list, left, right)

    return number_list


def merge(number_list, left_list, right_list):

    i, j, k = 0, 0, 0

    # compare and insert elements
    while i < len(left_list) and j < len(right_list):
        if left_list[i] < right_list[j]:
            number_list[k] = left_list[i]
            i += 1
        else:
            number_list[k] = right_list[j]
            j += 1
        
        k += 1
    
    # put remaining elements
    while i < len(left_list):
        number_list[k] = left_list[i]
        i += 1
        k += 1

    while j < len(right_list):
        number_list[k] = right_list[j]
        j += 1
        k += 1

print(B)        
merge_sort(B)
print(B)